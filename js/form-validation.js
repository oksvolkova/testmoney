$(document).ready(function () {
    $("#feedback-form").focusout(function () {
        $(this).validate();
        $("#name").rules("add", {
            required: true,
            minlength: 2,
            messages: {
                required: "Νecessary string",
                minlength: jQuery.validator.format("Please, at least 2 characters are necessary")
            }
        });

        $("#email").rules("add", {
            required: true,
            minlength: 2,
            messages: {
                required: "Νecessary string",
                minlength: jQuery.validator.format("Please, at least 2 characters are necessary")
            }
        });

        $("#message").rules("add", {
            required: true,
            minlength: 2,
            messages: {
                required: "Νecessary string",
                minlength: jQuery.validator.format("Please, at least 2 characters are necessary")
            }
        });
    });
});